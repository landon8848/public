export PATH=/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/bin:/sbin:/usr/local/sbin:~/Projects/scripts

export MANPATH=/usr/local/opt/grep/libexec/gnuman:$MANPATH

# ssh-agent forwarding
ssh-add -A 2>/dev/null

alias ll='ls -lah'
alias df='df -H'
alias qlf='qlmanage -p "$@" >& /dev/null' # MacOS command line quick look
alias ssh='ssh -A'
alias rdp='rdesktop -g 1920x1080'         # rdesktop and X11 need to be installed
alias vbm='/usr/local/bin/VBoxManage'     # VirtualBox CLI tools
alias cim='/usr/bin/vim'                  # common frustrating typo

# Terraform CLI & clearing local TF_VAR environment variables
alias tf='terraform'
alias tfreset='for var in `env | egrep -o ^TF_VAR_*[A-Za-z_]+`; do unset $var; done'

# git completion and status in $PS1 (from git contrib source)
. ~/git-completion.bash
. ~/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1
export PS1='\u@\[\e[37;1m\]\h \[\e[0m\]\W$(__git_ps1 " (%s)") \$ '

# bind cursor history search only if interactive, otherwise it will throw errors
iatest=$(echo "$-" | grep i 2>/dev/null)
if [[ $iatest > 0 ]]; then
bind  '"\e[A": history-search-backward' \
      '"\e[B": history-search-forward'
fi

export HISTTIMEFORMAT='%b %d %I:%M %p '     # using strftime format
export HISTCONTROL=ignoreboth               # ignoredups:ignorespace
export HISTIGNORE="history:pwd:exit:df:ll"
export HISTSIZE=10000

# iTerm integration & colors
export CLICOLOR=1
export TERM=xterm-256color
source ~/.iterm2_shell_integration.bash

# tmux aliases
alias tma='tmux attach -t "$@"'
alias tml='tmux list-sessions'
alias tmn='tmux new -s "$@"'

# Configure python environment (had to tweak this to accommodate Apple silicon)
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if [[ ( "system_profiler SPHardwareDataType | grep -q Intel" && "command -v pyenv 1>/dev/null 2>&1" ) ]]; then
  eval "$(pyenv init -)"
else
  eval "$(pyenv init --path)"
fi
alias python='/Library/Frameworks/Python.framework/Versions/Current/bin/python3'

# AWS
export AWS_PROFILE=