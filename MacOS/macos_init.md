# New Mac Setup Guide

### **Terminal**
#### First let's install a new terminal emulator. iTerm2 can be downloaded here:

[https://iterm2.com]() [^1]

[^1] _latest version at the time of this writing is iTerm2 3.4.4 (OS 10.14+)_

#### **System Defaults**

Disable "Hold Key for Alternate Characters", holding keys repeats the same keystroke

```
defaults write -g ApplePressAndHoldEnabled -bool false
```

Set the delay on the initial key repeat to _n_ milliseconds

```
defaults write -g InitialKeyRepeat -int 18
```

Subsequent keystrokes are delivered at _n_ milliseconds

```
defaults write -g KeyRepeat -int 2
```

Reduce the delay in Dock appearance when using auto-hide to _n_ milliseconds

```
defaults write com.apple.dock autohide-delay -float 0.02
```

##### **Change default shell** [^2]

```
$ echo $SHELL
/bin/zsh
$ chsh -s /bin/bash
Changing shell for lando.
Password for lando:
```

log out and then log back in

```
$ echo $SHELL
bin/bash
```

[^2] _The bash version that ships with MacOS is ancient (3.2.57), newer version should be installed using Homebrew (5.0.16 at the time of this writing, process is described in [Software](#software) section below)._

##### **~/.bash_proﬁle** - this sources .bashrc on login for portability

```
if [ -f ~/.bashrc ]; then
  source ~/.bashrc
fi
```

### **~/.bashrc**

##### Assorted aliases

```
alias ll='ls -lah'
alias qlf='qlmanage -p "$@" >& /dev/null'
alias rdp='rdesktop -g 1920x1080'
```

##### git-completion and git-prompt
git prompt will display the current state of $PWD if it is initialized in git, as well as the currently checked out branch. Furthermore, the git source includes scripts to allow bash completion of local/remote branches. These require the git-completion.bash and git-prompt.sh scripts available from public repo [https://github.com/git/git/tree/master/contrib/completion]()

Source the git scripts, and update bash prompt accordingly

```
. ~/<path to repo>/git-completion.bash # use cloned github repo path
. ~/<path to repo>/git-prompt.sh export GIT_PS1_SHOWDIRTYSTATE=1 # use cloned github repo path

export GIT_PS1_SHOWDIRTYSTATE=1
```

For git-prompt to work, you'll need to append this to your $PS1:
```
export PS1=$PS1\W$(__git_ps1 " (%s)")
```

##### .bash_history - start typing command and use up/down keys to search

```
iatest=$(echo "$-" | grep i 2>/dev/null)  # to prevent errors being thrown for 
if [[ $iatest > 0 ]]; then                # non-interactive shells
  bind '"\e[A": history-search-backward';
  bind '"\e[B": history-search-forward';
  bind '"^[[A": history-search-backward';
  bind '"^[[B": history-search-forward';
  bind '"^[OA": history-search-backward'
  bind '"^[OB": history-search-forward';
fi
```

##### for tmux users

```
alias tma='tmux attach -t "$@"'
alias tml='tmux list-sessions'
alias tmn='tmux new -s "$@"'
```

##### Terraform

If working with Terraform, the following aliases might be useful:

```
alias tf='terraform'
alias tfreset='for var in `env | egrep -o ^TF_VAR_*[A-Za-z_]+`; do unset $var; done'
```

### bash extensions and hidden binaries

Extensions are pretty self-explanatory, but oftentimes third-party applications (and even Apple themselves) will hide their most useful command line utilities in some deeply nested directory 

##### **Airport**

Built-in Apple utility for Wi-Fi interface (usually en0)

```
$ ln -s /System/Library/PrivateFrameworks/Apple80211.framework/Resources/airport /usr/local/bin/
$ airport -I
     agrCtlRSSI: -60
     agrExtRSSI: 0
    agrCtlNoise: -96
    agrExtNoise: 0
          state: running
        op mode: station
     lastTxRate: 360
        maxRate: 400
lastAssocStatus: 0
    802.11 auth: open
      link auth: wpa2-psk
          BSSID: fe:ec:da:ea:d6:ab
           SSID: rajnigandha
            MCS: 8
        channel: 36,1
```

##### **AnyConnect**

Cisco AnyConnect Client install hides CLI utilities under /opt/cisco directory; these are handy for scripting and diagnosis of connectivity

```
$ ln -s /opt/cisco/anyconnect/bin/vpn /usr/local/bin/
$ vpn stats | awk '/Client\ Address\ \(IPv4\)/{print $NF}'
192.168.42.37
```

### Software

##### **Homebrew Package Manager** | [https://brew.sh]()

```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

\***NOTE: M1 Macs will need to download and extract the Alternative Install**

[https://docs.brew.sh/Installation#untar-anywhere]()

```
$ mkdir homebrew && curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew
$ sudo mv ./homebrew /opt/
$ PATH=$PATH:/opt/homebrew/bin
```

Once Homebrew is installed, there are some essential utilities needed

* tmux | terminal multiplexer
* nmap | network exploration tool and security / port scanner
* bash | to replace the antiquated build MacOS ships with
* cask | emacs dependency management
* htop | a better top
* wget | internet file retriever
* geoip | legacy geoip
* irssi | modular IRC client
* watch | re-execute and redraw at n intervals
* speedtest-cli | CLI utility for speedtest
* pyenv | the least-painful python environment management for MacOS

##### **Fix bash**

```
$ sudo echo '/usr/local/bin/bash' >> /etc/shells
$ chsh -s /usr/local/bin/bash
```

##### pyenv .bashrc configuration

``` 
# Configure python environment
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
```

#### **Other**

BetterTouchTool | [https://folivora.ai]()

Keyboard Maestro | [https://www.keyboardmaestro.com/main/]()

Alfred 4 | [https://www.alfredapp.com]()