# Fix for stale iCloud Tabs in Safari (MacOS/iOS)

## ISSUE

I've been dealing with this issue for about a year now and it got to the point this morning that I decided to really have a shot at fixing it.
For anyone not familiar with this issue, here is how it presents:

1. Have iCloud Tabs enabled on iPhone and MacOS
2. Close tabs on iPhone, tabs remain in iCloud Tabs history on MacOS
3. Close tabs through MacOS iCloud Tabs, they reappear instantly, or after a few seconds
4. Continue accumulating stale tabs that are long since closed on your iPhone, until there are hundreds backlogged on Safari MacOS
5. Issue persists across any previously owned or newly registered Apple computers

Here are the troubleshooting steps I've taken in the past, all of which failed:

- restart devices
- disable Safari sync on all devices
- log out of iCloud after disabling sync
- wipe devices, log back into iCloud
- pretty much every permutation of the above that you can think of

All of these failed, even though other people have reported success in some instances. There may be some correct sequence in which to perform these steps that will fix the issue, but I've yet to find it.
Because of this, I decided to dig into the files related to iCloud Tabs syncing and take the appropriate measures. I'll write up a separate comment with the steps taken to resolve.

#### \*WARNING: I AM NOT AN APPLE DEVELOPER OR EMPLOYEE AND CAN NOT GUARANTEE AGAINST ANY UNDESIRED BEHAVIOR IN YOUR SYSTEM AFTER PERFORMING THESE STEPS. PLEASE ENSURE YOU HAVE BACKED UP YOUR DEVICES BEFORE PROCEEDING. DO NOT EXECUTE ANY COMMANDS ON YOUR SYSTEM UNLESS YOU HAVE AN UNDERSTANDING OF WHAT YOU ARE DOING AND ITS EFFECTS\*


## STEPS

1. Make a backup of your system, using Time Machine or a similar utility. Pay particular attention to make sure you include your `~/Library` directory in your backup. If you're not sure what this means or where to locate this directory, this is a good indicator that you may want to skip the remaining steps and get someone to assist you.
2. Close out of Safari on your devices. Navigate to your iCloud settings on your iPhone and Mac, and disable Safari syncing. Acknowledge any warnings it throws at you.
3. On your Mac, open up your Terminal.app, or whatever CLI application you primarily use.
4. Navigate to ~/Library/Safari/
</br>
</br>
	```
	$ cd ~/Library/Safari/
	```

5. Use the `sqlite3` utility to access the CloudTabs.db database. You can use whichever database utility you like, if you'd prefer to use something with a GUI. The `file` command will provide information about the .db file.
</br>
</br>
	```bash
	$ file CloudTabs.db
	CloudTabs.db: SQLite 3.x database, user version 6, last written using SQLite version 3036000

	$ sqlite3 CloudTabs.db
	
	```

6. At this point you'll be presented with the `sqlite3` CLI interface. Feel free to poke around if you need to familiarize yourself with the associated tables/schema. The particular table we are interested in is `cloud_tabs`.
</br>
</br>
```bash
SQLite version 3.36.0 2021-06-18 18:58:49
Enter ".help" for usage hints.
sqlite> .tables
cloud_tab_close_requests  cloud_tabs
cloud_tab_devices         metadata

sqlite> PRAGMA table_info(cloud_tabs);
0|tab_uuid|TEXT|1||1
1|system_fields|BLOB|1||0
2|device_uuid|TEXT|1||0
3|position|BLOB|1||0
4|title|TEXT|0||0
5|url|TEXT|1||0
6|is_showing_reader|BOOLEAN|0|0|0
7|is_pinned|BOOLEAN|0|0|0
8|reader_scroll_position_page_index|INTEGER|0||0
9|scene_id|TEXT|0||0

```

7. Now that we know which table to use and its schema, we can query it to narrow down the scope of our operations. Note: All SQL statements need to be terminated with a semicolon `;`
</br>
</br>
	a. Displays 100 entries from the table `cloud_tabs`
	```sql
	SELECT * FROM cloud_tabs LIMIT 100;
	```

	b. Displays the device UUIDs associated with the Apple devices currently syncing tabs to iCloud
	```sql
	SELECT DISTINCT device_uuid FROM cloud_tabs;
	```

	c. Shows all URLs being synced across your devices
	```sql
	SELECT url FROM cloud_tabs;
	```

8. The way in which I was able to determine which device UUID I wanted to remove from the table was to search for a URL that I knew to be long closed on my iPhone, but was still displaying in iCloud Tabs on my laptop. Luckily for me, this narrowed it down to a single UUID (`device_uuid` is not mine, just a randomly generated example).
</br>
</br>
	```sql
	SELECT device_uuid, url FROM cloud_tabs WHERE title LIKE '%meatspin%';
	470C2C15-B00B-444F-868B-98732EA3214|https://www.meatspin.com/index.html
	```

9. Remove all rows containing the offending UUID. **NOTE: I NEED TO STRESS THAT THIS IS A DESTRUCTIVE OPERATION AND IRREVERSIBLE WITHOUT A BACKUP OF THE DATABASE. PROCEED AT YOUR OWN RISK.**
</br>
</br>
	```sql
	DELETE FROM cloud_tabs WHERE device_uuid = '470C2C15-B00B-444F-868B-98732EA3214';
	```

10. We're done with `sqlite3`. You can close out of the application.
</br>
</br>
	```bash
	sqlite> .quit
	```

At this point, you should be pretty much done. Go ahead and re-enable Safari sync in iCloud settings and enjoy your newly decluttered open tabs menu. I honestly don't know if this is a permanent solution, but it's been a couple of hours now and I've yet to see any stale tabs return, nor any adverse effects from my actions. In any event, if they do pop up again I can only assume it's a backend issue on Apple's part, and I would hope it's fixed soon. It's been months and months of dealing with this though so I would not hold my breath.