# README #

There's not a lot here right now. I'm in the process of sanitizing and modifying my private repository for security and portability.

### What will I find here? ###

+ Scripts for efficiency
	- bash
	- python
	- perl
	- awk
+ Handy yet oftentimes complex regex
	- PCRE
	- POSIX