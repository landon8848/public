#! /usr/bin/env python
# Created by Lando
# Make sure you use python >= 3.7

import random

first = ["Client", "Customer", "Technical", "Global", "Universal", "Support", "International", "Tech"]
second = ["Operations", "Support", "Assistance", "Solutions", "Analysis"]
third = ["Group", "Representatives", "Center", "Services", "Ops", "Folks", "Dude/ttes"]

print(random.choice(first) + ' ' + random.choice(second) + ' ' + random.choice(third))
